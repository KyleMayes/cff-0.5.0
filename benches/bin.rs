// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate cff;
extern crate microbench;
extern crate sfnt;

use std::fs::{File};
use std::io::{Read};
use std::path::{Path};

use cff::{Cff};

use microbench::{Options};

use sfnt::{Sfnt};

pub fn with_cff<P: AsRef<Path>, F: Fn(&[u8])>(path: P, f: F) {
    let mut file = File::open(path).unwrap();
    let mut bytes = vec![];
    file.read_to_end(&mut bytes).unwrap();
    let sfnt = Sfnt::parse(&bytes).unwrap();
    f(sfnt.find(b"CFF ").unwrap().1);
}

fn bench_parse_cff() {
    with_cff("tests/resources/NotoSansCJKjp-Regular.otf", |bytes| {
        let options = Options::default();
        microbench::bench(&options, "parse_cff_NotoSansCJKjp-Regular", || {
            Cff::parse(bytes).unwrap()
        });
    });
}

fn bench_glyphs() {
    with_cff("tests/resources/NotoSansCJKjp-Regular.otf", |bytes| {
        let cff = Cff::parse(bytes).unwrap();
        let options = Options::default();
        microbench::bench(&options, "glyphs_NotoSansCJKjp-Regular", || {
            cff.parse_glyphs(0).unwrap().unwrap()
        });
    });
}

fn bench_charstring() {
    with_cff("tests/resources/NotoSansCJKjp-Regular.otf", |bytes| {
        let cff = Cff::parse(bytes).unwrap();
        let glyphs = cff.parse_glyphs(0).unwrap().unwrap();
        let options = Options::default();
        microbench::bench(&options, "charstring_NotoSansCJKjp-Regular", || {
            for index in 0..glyphs.charstrings.len() {
                glyphs.parse_charstring(index).unwrap().unwrap();
            }
        });
    });
}

fn bench_charstring_iter() {
    with_cff("tests/resources/NotoSansCJKjp-Regular.otf", |bytes| {
        let cff = Cff::parse(bytes).unwrap();
        let glyphs = cff.parse_glyphs(0).unwrap().unwrap();
        let (charstring, _) = glyphs.parse_charstring(737).unwrap().unwrap();
        let options = Options::default();
        microbench::bench(&options, "charstring_iter_NotoSansCJKjp-Regular", || {
            for operation in charstring.operations() {
                operation.unwrap();
            }
        });
    });
}

fn main() {
    bench_parse_cff();
    bench_glyphs();
    bench_charstring();
    bench_charstring_iter();
}
