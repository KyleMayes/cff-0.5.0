// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate cff;
extern crate sfnt;

use std::fs::{File};
use std::io::{Read};

use cff::{Cff};
use cff::glyphs::charstring::{Operation, Point};

use sfnt::{Sfnt};

fn main() {
    // Read the font file into memory.
    let mut file = File::open("tests/resources/SourceSansPro-It.otf").unwrap();
    let mut bytes = vec![];
    file.read_to_end(&mut bytes).unwrap();

    // Parse the font file and find the CFF table in the font file.
    let sfnt = Sfnt::parse(&bytes).unwrap();
    let (_, bytes) = sfnt.find(b"CFF ").unwrap();

    // Parse the CFF table.
    let cff = Cff::parse(&bytes).unwrap();

    // Parse the glyphs for the first font.
    let glyphs = cff.parse_glyphs(0).unwrap().unwrap();

    // Find the charstring for the ".notdef" glyph.
    let (charstring, _) = glyphs.parse_charstring(0).unwrap().unwrap();

    // Parse and collect the operations in the charstring.
    let operations = charstring.operations().collect::<Result<Vec<_>, _>>().unwrap();

    assert_eq!(&operations[..6], &[
        Operation::Width(92),
        Operation::HStem(0, 56),
        Operation::HStem(604, 660),
        Operation::VStem(36, 622),
        Operation::MoveTo(Point { x: 36, y: 0 }),
        Operation::LineTo(Point { x: 492, y: 0 }),
    ]);
}
