## [0.5.0] - 2019-01-02

Changed a whole bunch of stuff.<br>
No one uses this library yet, so I didn't bother recording the changes.

## [0.4.0] - 2018-12-27

### Changed
- Bumped `sfnt` version to `0.10.0`
- Bumped `tarrasque` version to `0.9.0`

### Fixed
- Removed some possible panics

## [0.3.1] - 2018-12-11

### Changed
- Bumped `tarrasque` version to `0.7.0`

## [0.3.0] - 2018-12-08

### Changed
- Bumped `sfnt` version to `0.8.0`
- Bumped `tarrasque` version to `0.6.0`
- Upgrade to Rust 2018

## [0.2.0] - 2018-06-17

### Changed
- Moved `parse_cff` to `Cff::parse`
- Moved `parse_glyphs` to `Glyphs::parse`
- Bumped `sfnt` version to `0.7.0`
- Bumped `tarrasque` version to `0.5.0`

## [0.1.0] - 2018-05-20
- Initial release
