// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! CFF type 2 charstring numbers.

use tarrasque::{Extract, ExtractResult, Stream};

/// A `16.16` fixed-point number.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Fixed(pub u32);

impl<'s> Extract<'s, ()> for Fixed {
    #[inline]
    fn extract(stream: &mut Stream<'s>, _: ()) -> ExtractResult<'s, Self> {
        stream.extract(()).map(Fixed)
    }
}

impl Into<f32> for Fixed {
    #[inline]
    fn into(self) -> f32 {
        Into::<f64>::into(self) as f32
    }
}

impl Into<f64> for Fixed {
    #[inline]
    fn into(self) -> f64 {
        f64::from(self.0) / 65536.0
    }
}

/// A type 2 charstring number.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Number {
    /// A `16.16` fixed-point number.
    Fixed(Fixed),
    /// An integer.
    Integer(i16),
}

impl<'s> Extract<'s, u8> for Number {
    #[inline]
    fn extract(stream: &mut Stream<'s>, byte: u8) -> ExtractResult<'s, Self> {
        match byte {
            28 => Ok(Number::Integer(stream.extract::<i16, _>(())?)),
            255 => Ok(Number::Fixed(stream.extract(())?)),
            b if b >= 32 && b <= 246 => Ok(Number::Integer(i16::from(b) - 139)),
            b if b >= 247 && b <= 250 => {
                let high = i16::from(b - 247) * 256;
                let low = i16::from(stream.extract::<u8, _>(())?);
                Ok(Number::Integer(high + low + 108))
            },
            b if b >= 251 && b <= 254 => {
                let high = i16::from(b - 251) * 256;
                let low = i16::from(stream.extract::<u8, _>(())?);
                Ok(Number::Integer(-high - low - 108))
            },
            _ => unreachable!(),
        }
    }
}

impl Into<i16> for Number {
    #[inline]
    fn into(self) -> i16 {
        match self {
            Number::Fixed(fixed) => Into::<f32>::into(fixed) as i16,
            Number::Integer(integer) => integer,
        }
    }
}

impl Into<f32> for Number {
    #[inline]
    fn into(self) -> f32 {
        match self {
            Number::Fixed(fixed) => fixed.into(),
            Number::Integer(integer) => f32::from(integer),
        }
    }
}

impl Into<f64> for Number {
    #[inline]
    fn into(self) -> f64 {
        match self {
            Number::Fixed(fixed) => fixed.into(),
            Number::Integer(integer) => f64::from(integer),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_extract_number() {
        assert_eq!(Stream(&[1, 2]).extract(28), Ok(Number::Integer(0x0102)));
        assert_eq!(Stream(&[]).extract(32), Ok(Number::Integer(-107)));
        assert_eq!(Stream(&[]).extract(246), Ok(Number::Integer(107)));
        assert_eq!(Stream(&[0]).extract(247), Ok(Number::Integer(108)));
        assert_eq!(Stream(&[255]).extract(250), Ok(Number::Integer(1131)));
        assert_eq!(Stream(&[0]).extract(251), Ok(Number::Integer(-108)));
        assert_eq!(Stream(&[255]).extract(254), Ok(Number::Integer(-1131)));

        let fixed = Stream(&[1, 2, 3, 4]).extract(255);
        assert_eq!(fixed, Ok(Number::Fixed(Fixed(0x0102_0304))));
    }
}
